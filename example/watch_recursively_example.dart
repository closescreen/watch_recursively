import 'dart:io';

import 'package:watch_recursively/watch_recursively.dart';

void main() async {
  var dir = Directory('.');
  var stream = await dir.watchRecursively();
  stream.listen((event)=>print(event));
}
